let inp = document.getElementById("price");
let under = document.getElementById("under");
let over = document.getElementById("over");
let overButton = document.createElement('button');
let overText = document.createElement('span');
inp.addEventListener("blur", function() {
    if (parseInt(inp.value) < 0) {
        inp.classList.add('invalid');
        inp.style.color = "";
        under.innerHTML = "Please enter correct price";
    } else {
        inp.classList.remove('invalid');
        under.innerHTML = "";
        inp.style.color = "limegreen";
        overText.innerHTML = "Текущая цена: $"+ inp.value + " ";
        over.append(overText);
        overButton.innerHTML = 'x';
        over.append(overButton);
    }
});
overButton.addEventListener("click", function () {
    overText.remove();
    overButton.remove();
    inp.style.color = "";
    inp.value = "";
});


