let img = document.getElementById('image-to-show');
let stop = document.getElementById('stop');
let cont = document.getElementById('cont');
let counter = document.getElementById('counter');
let i = 0;
let j = 9.8;
let timer;
let timerCounter;
document.addEventListener("load", changeImg());
document.addEventListener("load", countdown());

function changeImg () {
    if (i <= 3) {
        i++;
    } else {
        i = 1;
    }
    img.setAttribute('src', './img/'+(i)+'.jpg');
    timer = setTimeout(changeImg, 10000);
}

function countdown() {
    if (j >= 0.1) {
        j = j - 0.1;
    } else {
        clearTimeout(timerCounter);
        j = 9.8;
    }
    counter.innerText = 'Timer: ' + j.toFixed(1);
    timerCounter = setTimeout(countdown, 100)
}

stop.onclick = function () {
    clearTimeout(timer);
    clearTimeout(timerCounter)
};
cont.onclick = function () {
    setTimeout(changeImg, 10000);
    setTimeout(countdown, 100);
    j = 9.8;
};



