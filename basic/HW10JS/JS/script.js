
let form = document.getElementById('form');
let btn = document.getElementById('btn');
let input;
let inputParent;
let div;
form.addEventListener("click", changeVisible);
function changeVisible (event) {

    if (event.target.tagName == 'I') {
        if (event.target.className === 'fas fa-eye icon-password') {
            inputParent = event.target.closest('label');
            input = inputParent.querySelector('input');
            input.type = "text";
            event.target.className = "fas fa-eye-slash icon-password";
        } else {
            inputParent = event.target.closest('label');
            input = inputParent.querySelector('input');
            input.type = "password";
            event.target.className = "fas fa-eye icon-password";
        }
    }
}
btn.addEventListener("click", submit);
function submit(event) {
    if (event.target.tagName == 'BUTTON') {
        if (document.getElementsByTagName('div')[0] !== undefined) {
            document.getElementsByTagName('div')[0].remove();
        }
        if (document.getElementById('first-field').value === document.getElementById('second-field').value) {
            alert("You are welcome");
        } else {
            div = document.createElement("div");
            div.innerHTML = 'Нужно ввести одинаковые значения';
            document.getElementById('second-field').after(div);
        }
    }
}


