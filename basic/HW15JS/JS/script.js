
$(document).ready(function () {
    $(".navbar a").click(function () {
        let target = $(this).attr("href"),
            scrlTop = $(target).offset().top - 70;
        $("body, html").animate({scrollTop: scrlTop}, 1000, 'swing');
    });

    let upBtn = $('.go-up-btn');
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 390) {
            upBtn.fadeIn();
        } else {
            upBtn.fadeOut();
        }
    });
    $(upBtn).click(function () {
        $("body, html").animate({scrollTop: 0}, 1000, 'swing');
    });

    let hideBtn = $('.toggle-btn');
    $(hideBtn).click(function () {
        $('#hot-news').slideToggle(1000)
    })
});



