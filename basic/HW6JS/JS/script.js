
function filterBy(data, dataType) {
    let newData = data.filter(function(dataElement) {
        return (typeof(dataElement) !== dataType);
    });
    return(newData);
}
const result = filterBy(['hello', 'world', 23, 45, 11, '23', {name: "Ed", age: 20}, null, true, false, undefined], 'string');
console.log(result);