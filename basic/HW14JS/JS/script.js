let tabsText = $(".content-text");
let activeTab = $(".tabs-title.active");
$("#tabs").on("click", "li", function (event) {
        for (let i = 0; i < tabsText.length; i++) {
            $(tabsText[i]).attr("hidden", true);
        }
        $(activeTab).toggleClass("active");
        activeTab = event.target;
        $(activeTab).toggleClass("active");
        let id = $(event.target).attr('data-id');
        let tabText = document.getElementById(id);
        $(tabText).attr("hidden", false);
    }
);

