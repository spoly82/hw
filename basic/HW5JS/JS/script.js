
function createNewUser() {
    let firstName = prompt("enter first name");
    let lastName = prompt("enter last name");
    let birthday = prompt("enter birthday", "01.01.1900");
    let birthdaySplited = birthday.split('.');
    let newUser = {
        firstName,
        lastName,
        birthday,
    };
    Object.defineProperties (newUser, {
        "getLogin": {
            get: () => {
                return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase()
            }
        }
    });
    Object.defineProperties (newUser, {
        "getAge": {
            get: () => {
                let nowDate = new Date();
                return nowDate.getFullYear() - birthdaySplited[2]
            }
        }
    });
    Object.defineProperties (newUser, {
        "getPassword": {
            get: () => {
                return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase() + birthdaySplited[2]
            }
        }
    });
    return (newUser);
}
const NewUser1 = createNewUser();
console.log(NewUser1);
console.log(NewUser1.getLogin);
console.log(NewUser1.getAge);
console.log(NewUser1.getPassword);


