let tabs = document.getElementById("tabs");
let tabsText = document.getElementsByClassName("content-text");
let activeTab;
tabs.addEventListener('click', changeActive);
function changeActive(event) {
    for (let i = 0; i < tabsText.length; i++) {
        tabsText[i].setAttribute('hidden', true)
    }
    if(event.target.tagName == 'LI') {
        if(activeTab !== undefined) {
            activeTab.classList.remove('active');
        }
        activeTab = event.target;
        activeTab.classList.add('active');
        let id = event.target.dataset.id;
        let tabText = document.getElementById(id);
        tabText.removeAttribute('hidden');
    }
}
