
let btns = document.getElementsByClassName("btn");
document.addEventListener('keydown', changeActive);
function changeActive(event) {
    for (i = 0; i < btns.length; i++) {
        if(event.code === btns[i].dataset.keycode) {
            btns[i].style.backgroundColor = 'blue';
        } else {
            btns[i].style.backgroundColor = '';
        }
    }
}
