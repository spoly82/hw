let btn = document.getElementById('change');
let change = function () {
    document.getElementsByTagName('body')[0].style.backgroundColor = 'black';
    btn.style.backgroundColor = 'white';
    btn.style.color = 'black';
    document.getElementById('ra').style.color = 'white';
    document.getElementsByTagName('p')[0].style.color = 'white';
    document.getElementsByTagName('p')[1].style.color = 'white';
    document.getElementById('lm').style.border = '1px solid white';
    let lma = document.getElementsByClassName('lma');
    for (i = 0; i < lma.length; i++) {
        lma[i].classList.remove('nochange1');
        lma[i].classList.add('change');
    }
    let bm = document.getElementsByClassName('bm');
    for (i = 0; i < bm.length; i++) {
        bm[i].classList.remove('nochange2');
        bm[i].classList.add('change');
    }
    document.getElementById('copy').style.color = 'white'
};
window.onload = function () {
    if (localStorage.getItem('change') === 'black') {
        change();
    }
    btn.onclick = function () {
        if (localStorage.getItem('change') === null) {
            change();
            localStorage.setItem('change', 'black');
        } else {
            document.getElementsByTagName('body')[0].style.backgroundColor = '';
            btn.style.backgroundColor = '';
            btn.style.color = '';
            document.getElementById('ra').style.color = '';
            document.getElementsByTagName('p')[0].style.color = '';
            document.getElementsByTagName('p')[1].style.color = '';
            document.getElementById('lm').style.border = '';
            let lma = document.getElementsByClassName('lma');
            for (i = 0; i < lma.length; i++) {
                lma[i].classList.remove('change');
                lma[i].classList.add('nochange1');
            }
            let bm = document.getElementsByClassName('bm');
            for (i = 0; i < bm.length; i++) {
                bm[i].classList.remove('change');
                bm[i].classList.add('nochange2');
            }
            document.getElementById('copy').style.color = '';
            localStorage.removeItem('change');
        }

    }
};
