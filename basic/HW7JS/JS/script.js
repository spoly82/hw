let ul = document.createElement('ul');
document.body.append(ul);
function changeData(data) {
    data.map(function(element) {
        let li = document.createElement('li');
        if (typeof element == 'object' && element != null) {
            let subUl = document.createElement('ul');
            ul.append(subUl);
            element.map(function (subElement) {
                let subLi = document.createElement('li');
                subLi.innerHTML = `${subElement}`;
                subUl.append(subLi);
            })

        } else {
            li.innerHTML = `${element}`;
            ul.append(li);
        }
        });
}
changeData(['1', '2', '3', ['a', 'b', 'c'], null, 'sea', 'user', 23]);




