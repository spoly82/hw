let serviceMenu = document.getElementById('service-menu');
let activeServiceMenu = document.getElementsByClassName('active')[0];
const triangle = document.getElementById('triangle');
const serviceImg = document.getElementById('service-img');
let activeServiceText = document.getElementById('web-design');
let id;

serviceMenu.addEventListener('click', changeActive);

function changeActive(event) {
    if(event.target.tagName == 'LI') {
        activeServiceMenu.classList.remove('active');
        activeServiceMenu = event.target;
        id = event.target.dataset.id;
    } else {
        if (event.target.closest('LI') != null) {
            activeServiceMenu.classList.remove('active');
            activeServiceMenu = event.target.closest('LI');
            id = event.target.closest('LI').dataset.id;
        } else {
            return
        }
    }
    activeServiceMenu.classList.add('active');
    activeServiceText.setAttribute('hidden', true);
    activeServiceText = document.getElementById(id);
    activeServiceText.removeAttribute('hidden');
    switch (id) {
        case 'web-design':
            triangle.style.left = '49px';
            serviceImg.setAttribute('src', 'img/service1.png');
            break;
        case 'graphic-design':
            triangle.style.left = '163px';
            serviceImg.setAttribute('src', 'img/service2.jpg');
            break;
        case 'online-support':
            triangle.style.left = '277px';
            serviceImg.setAttribute('src', 'img/service3.jpg');
            break;
        case 'app-design':
            triangle.style.left = '391px';
            serviceImg.setAttribute('src', 'img/service4.jpg');
            break;
        case 'online-marketing':
            triangle.style.left = '505px';
            serviceImg.setAttribute('src', 'img/service5.jpg');
            break;
        case 'seo-service':
            triangle.style.left = '619px';
            serviceImg.setAttribute('src', 'img/service6.jpg');
            break;
    }
}
