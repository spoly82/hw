const slider = document.querySelector('.slider');
const mainFoto = document.getElementById('main-foto');
let activeSlogan = document.getElementById('Hasan');
let activeName = document.querySelector('.team-section-name > p:first-child');
let activePosition = document.querySelector('.team-section-name > p:last-child');

slider.addEventListener('click', chooseTeammate);

function chooseTeammate(event) {
    if(event.target.tagName == 'IMG') {
        activeTeammate = event.target;
        id = event.target.dataset.id;
    } else {
        if (event.target.closest('IMG') != null) {
            // activeTeammate.classList.remove('active');
            activeTeammate = event.target.closest('IMG');
            id = event.target.closest('IMG').dataset.id;
        } else {
            return
        }
    }
    activeSlogan.setAttribute('hidden', true);
    activeSlogan = document.getElementById(id);
    activeSlogan.removeAttribute('hidden');
    switch (id) {
        case 'Teresa':
            activeName.innerText = 'Teresa Mayer';
            activePosition.innerText = 'Tester';
            mainFoto.setAttribute('src', 'img/team2.png');
            break;
        case 'Frank':
            activeName.innerText = 'Frank Sinatra';
            activePosition.innerText = 'Content Manager';
            mainFoto.setAttribute('src', 'img/team3.png');
            break;
        case 'Hasan':
            activeName.innerHTML = 'Hasan Ali';
            activePosition.innerText = 'UX Designer';
            mainFoto.setAttribute('src', 'img/team1.png');
            break;
        case 'Tanya':
            activeName.innerText = 'Tanya Roser';
            activePosition.innerText = 'Database Administrator';
            mainFoto.setAttribute('src', 'img/team4.png');
            break;
        case 'John':
            activeName.innerText = 'John Brown';
            activePosition.innerText = 'Webmaster';
            mainFoto.setAttribute('src', 'img/team5.png');
            break;
        case 'Max':
            activeName.innerText = 'Max Mitchel';
            activePosition.innerText = 'Lead Developer';
            mainFoto.setAttribute('src', 'img/team6.png');
            break;
    }
}
