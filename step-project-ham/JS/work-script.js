const workMenu = document.getElementById('work-menu');
const workSection = document.getElementById('work-section-imgs');
const workBlocks = document.querySelectorAll('#work-section-imgs > div');
const workBtn = document.getElementById('work-btn');
let additionalWorkBlocks;
const loader = document.getElementById('bubbles');

workMenu.addEventListener('click', selectBlockById);

function selectBlockById(event) {
    if(event.target.tagName == 'LI') {
        selectedWorkMenu = event.target;
        id = event.target.dataset.id;
    } else {
        if (event.target.closest('LI') != null) {
            selectedWorkMenu = event.target.closest('LI');
            id = event.target.closest('LI').dataset.id;
        } else {
            return
        }
    }
    if (id == 'all') {
        for (let i=0; i < workBlocks.length; i++) {
            workBlocks[i].removeAttribute('hidden');
        }
    } else {

        for (let i = 0; i < workBlocks.length; i++) {
            if (workBlocks[i].className != id) {
                workBlocks[i].setAttribute('hidden', 'true')
            } else {
                workBlocks[i].removeAttribute('hidden');
            }
        }
    }
}

workBtn.addEventListener('click', loadMore);

function loadMore(event) {
    if (workBtn.classList.contains('clicked')) {
        workBtn.classList.add('on-load');
        loader.classList.remove('on-load');
        setTimeout(function () {
            loader.classList.add('on-load');
            additionalWorkBlocks = document.querySelectorAll( '.second-click');
            workSection.classList.add('additional-padding');
            for (let i=0; i < additionalWorkBlocks.length; i++) {
                additionalWorkBlocks[i].classList.remove('second-click')
            }
        }, 2000)
    } else {
        workBtn.classList.add('on-load');
        loader.classList.remove('on-load');
        setTimeout(function () {
            workBtn.classList.remove('on-load');
            workBtn.classList.add('clicked');
            loader.classList.add('on-load');
            additionalWorkBlocks = document.querySelectorAll( '.first-click');
            for (let i=0; i < additionalWorkBlocks.length; i++) {
                additionalWorkBlocks[i].classList.remove('first-click')
            }
        }, 2000)
    }
}