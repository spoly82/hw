import React, {useContext} from 'react';
import {ProductsList} from '../ProductsList';
import {ProductCart} from '../ProductCart';
import {ContextApp} from "../../../../store/reducer";
import {Navbar} from "../../../../shared/components/Navbar";

export const ProductApp = () => {
    const {state} = useContext(ContextApp);
    localStorage.setItem('cart', JSON.stringify(state.productsAddedToCart));

    return (
        <>
            <Navbar/>
            <ProductsList/>
            {state.cartIsOpen ? <ProductCart/> : null}
        </>
    )
};
