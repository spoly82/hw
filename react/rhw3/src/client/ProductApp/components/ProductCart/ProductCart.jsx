import React, {useContext} from "react";
import {ContextApp} from "../../../../store/reducer";
import {ADD_TO_CART, CLOSE_CART, DECREMENT_PRODUCT_COUNT, REMOVE_FROM_CART} from "../../../../store/actions";

export const ProductCart = () => {
    const {state, dispatch} = useContext(ContextApp);
    let totalProductPrice = 0;
    if (state.productsAddedToCart.length > 0) {
        totalProductPrice = state.productsAddedToCart.reduce((sum, current) => sum + current.count*current.price, 0).toFixed(2);
    }

    const productsAddedToCartElements = state.productsAddedToCart.map(element =>
        <tr key={element.id}>
            <td>{element.name}</td>
            <td>{element.price}</td>
            <td>
                <div className="input-group">
                    <button className="minus-item input-group-addon btn btn-primary" onClick={() => dispatch({type: DECREMENT_PRODUCT_COUNT, payload: element})}>-</button>
                    <input type="number" className="item-count form-control" value={element.count}/>
                    <button className="plus-item btn btn-primary input-group-addon" onClick={() => dispatch({type: ADD_TO_CART, payload: element})}>+</button>
                </div>
            </td>
            <td>
                <button className="delete-item btn btn-danger" onClick={() => dispatch({type: REMOVE_FROM_CART, payload: element})}>X</button>
            </td>
            <td>{element.price*element.count}</td>
        </tr>
    );

    return (
        <div >
            <div className="modal-overlay">
                <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Cart</h5>
                        <button type="button" className="close" onClick={() => dispatch({type: CLOSE_CART})}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <table className="show-cart table">
                            {productsAddedToCartElements}
                        </table>
                        <div>Total price: ${totalProductPrice}<span className="total-cart"></span></div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={() => dispatch({type: CLOSE_CART})}>Close</button>
                        <button type="button" className="btn btn-primary">Order now</button>
                    </div>
                </div>
            </div>
            </div>
        </div>
    )
};