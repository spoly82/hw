import React, {useContext} from 'react'
import {ContextApp} from "../../../../store/reducer";
import {ADD_TO_CART} from "../../../../store/actions";

export const ProductsList = () => {
    const {state, dispatch} = useContext(ContextApp);
    const productsElements = state.products.map(element => {

        const handleClick = (e) => {
            e.preventDefault();
            dispatch({type: ADD_TO_CART, payload: element})
        };

        return (
            <div className="col" key={element.id}>
                <div className="card" style={{width: 20 +"rem"}}>
                    <img className="card-img-additional" src={element.src} alt="Card image cap" />
                    <div className="card-block">
                        <h4 className="card-title">{element.name}</h4>
                        <p className="card-text">Price: ${element.price}</p>
                        <a href="#" className="add-to-cart btn btn-primary" onClick={handleClick}>Add to cart</a>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div className="container">
            <div className="row">
                {productsElements}
            </div>
        </div>
    )
};
