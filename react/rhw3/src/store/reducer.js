import React from "react";
import {ADD_TO_CART, CLEAR_CART, CLOSE_CART, DECREMENT_PRODUCT_COUNT, OPEN_CART, REMOVE_FROM_CART} from './actions'

export const ContextApp = React.createContext();

let localItemsInCart = JSON.parse(localStorage.getItem('cart'));
const productsAddedToCart = localItemsInCart || [];

export const initialState = {
    products: [
        {
            name: "Orange",
            price: 0.5,
            src: "http://www.azspagirls.com/files/2010/09/orange.jpg",
            id: 1
        },
        {
            name: "Banana",
            price: 1.22,
            src: "http://images.all-free-download.com/images/graphicthumb/vector_illustration_of_ripe_bananas_567893.jpg",
            id: 2
        },
        {
            name: "Lemon",
            price: 5,
            src: "https://3.imimg.com/data3/IC/JO/MY-9839190/organic-lemon-250x250.jpg",
            id: 3
        },
    ],
    productsAddedToCart,
    cartIsOpen: false,
};

export const changeCartList = (state, action) => {

    let newProductsAddedToCart;
    let currentProductIndex;

    switch (action.type) {
        case ADD_TO_CART:
            newProductsAddedToCart = [...state.productsAddedToCart];
            currentProductIndex = newProductsAddedToCart.findIndex(item => item.id === action.payload.id);
            if (currentProductIndex < 0) {
                newProductsAddedToCart.push({...action.payload, count: 1});
            } else {
                newProductsAddedToCart[currentProductIndex] = {
                    ...newProductsAddedToCart[currentProductIndex], count: newProductsAddedToCart[currentProductIndex].count+1
                }
            }
            return {
                ...state,
                productsAddedToCart: newProductsAddedToCart,
            };
        case DECREMENT_PRODUCT_COUNT:
            newProductsAddedToCart = [...state.productsAddedToCart];
            currentProductIndex = newProductsAddedToCart.findIndex(item => item.id === action.payload.id);
            if (newProductsAddedToCart[currentProductIndex].count > 1) {
                newProductsAddedToCart[currentProductIndex] = {
                    ...newProductsAddedToCart[currentProductIndex], count: newProductsAddedToCart[currentProductIndex].count-1
                }
            } else {
                newProductsAddedToCart.splice(currentProductIndex, 1);
            }
            return {
                ...state,
                productsAddedToCart: newProductsAddedToCart,
            };
        case REMOVE_FROM_CART:
            newProductsAddedToCart = state.productsAddedToCart.filter(
                (item) => item.id !== action.payload.id
            );
            return {
                ...state,
                productsAddedToCart: newProductsAddedToCart,
            };
        case CLEAR_CART:
            return {
                ...state,
                productsAddedToCart: [],
            };
        case OPEN_CART:
            return {
                ...state,
                cartIsOpen: true,
            };
        case CLOSE_CART:
            return {
                ...state,
                cartIsOpen: false,
            };
        default:
            break;
    }
};