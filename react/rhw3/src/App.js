import React, {useReducer} from 'react';
import './App.css';
import { ProductApp } from './client/ProductApp/components/ProductApp';
import {ContextApp, changeCartList, initialState} from "./store/reducer";

function App() {
    const [state, dispatch] = useReducer(changeCartList, initialState);
    return (
        <ContextApp.Provider value={{dispatch, state}}>
            <ProductApp/>
        </ContextApp.Provider>
    );
}

export default App;


