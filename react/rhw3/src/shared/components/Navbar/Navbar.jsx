import React, {useContext} from "react";
import {ContextApp} from "../../../store/reducer";
import {CLEAR_CART, OPEN_CART} from "../../../store/actions";

export const Navbar = () => {
    const {state, dispatch} = useContext(ContextApp);
    let totalProductCount = 0;
    if (state.productsAddedToCart.length > 0) {
        totalProductCount = state.productsAddedToCart.reduce((sum, current) => sum + current.count, 0);
    }

    return (
        <nav className="navbar navbar-inverse bg-inverse fixed-top bg-faded navbar-additional">
            <div className="row">
                <div className="col">
                    <button type="button" className="btn btn-primary" onClick={() => dispatch({type: OPEN_CART})}>Cart (<span className="total-count">{totalProductCount}</span>)</button>
                    <button className="clear-cart btn btn-danger" onClick={() => dispatch({type: CLEAR_CART})}>Clear Cart</button>
                </div>
            </div>
        </nav>
    )
};