import React from 'react';
import './App.css';
import {Header} from "./Components/Header/Header";
import {Content} from "./Components/Content/Content";
import {Footer} from "./Components/Footer/Footer";

const headerItem = {
    url: '../images/junglebook.png',
    name: 'The Jungle Book',
    genre: ['Adventure', 'Drama', 'Family', 'Fantasy'],
    filmLength: '1h 46m',
    mark: 4.8,
};

function App() {
  return (
      <>
        <Header
            headerItem={headerItem}
        />
        <Content />
        <Footer />
      </>
  );
}

export default App;
