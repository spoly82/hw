import React from "react";
import './Footer.css'
import {Logo} from "../Shared/Logo/Logo";
import {Navbar} from "../Shared/Navbar/Navbar";
import {SocialIcons} from "../Shared/SocialIcons/SocialIcons";

const socialIconsNames = ['facebook', 'twitter', 'pinterest-p', 'instagram', 'youtube'];

export const Footer = () => {
    return (
        <footer>
            <div className='footer-wrapper'>
                <div className='footer-navbar'>
                    <Navbar
                        liItems={['About', 'Terms of Service', 'Contact']}
                        locationStyle={'footer-navbar-style'}
                    />
                </div>
                <div className='footer-logo'>
                    <Logo
                        color={'#0aaee4'}
                        fontSize={'25px'}
                    />
                </div>
                <SocialIcons
                    socialIconsNames={socialIconsNames}
                />
            </div>
            <div className='copyright-container'>
                <span className={'copyright-text'}>Copyright © 2017 </span>
                <Logo
                    color={'#a7aaab'}
                    fontSize={'14px'}
                />
                <span className={'copyright-text'}>. All Rights Reserved.</span>
            </div>
        </footer>
    )
};