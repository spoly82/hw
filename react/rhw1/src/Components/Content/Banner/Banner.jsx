import React from 'react';
import './Banner.css';
import {Button} from "../../Shared/Button/Button";

export const Banner = (props) => {
    return (
        <div className='banner'>
            <div className='banner-inner-container'>
                <p className='banner-article'>Receive information on the latest hit movies straight to your inbox.</p>
                <Button
                    colorStyle={'blue-button'}
                    sizeStyle={'big-size-button'}
                    text={'Subscribe!'}
                />
            </div>
        </div>
    )
};