import React from 'react';
import {Banner} from "./Banner/Banner";
import './Content.css'
import {MainItems} from "./MainItems/MainItems";
import {Navbar} from "../Shared/Navbar/Navbar";

const mainItems = [
    {
        url: '../images/fantastic_beasts.png',
        top: '-102px',
        left: '-34px',
        name: 'Fantastic Beasts...',
        mark: 4.7,
        genre: ['Adventure', 'Family', 'Fantasy'],
        id: 1
    },
    {
        url: '../images/creed.png',
        top: '0px',
        left: '-221px',
        name: 'AssAssin’s Creed',
        mark: 4.2,
        genre: ['Action', 'Adventure', 'Fantasy'],
        id: 2
    },
    {
        url: '../images/nowyouseeme.png',
        top: '0px',
        left: '-214px',
        name: 'Now you see me 2',
        mark: 4.4,
        genre: ['Action', 'Adventure', 'Comedy'],
        id: 3
    },
    {
        url: '../images/tarzan.png',
        top: '0px',
        left: '0px',
        name: 'The Legend of Ta...',
        mark: 4.3,
        genre: ['Action', 'Adventure', 'Drama'],
        id: 4
    },
    {
        url: '../images/the-bfg.png',
        top: '0px',
        left: '-200px',
        name: 'The BFG',
        mark: 3.2,
        genre: ['Adventure', 'Family', 'Fantasy'],
        id: 5
    },
    {
        url: '../images/independence-day.png',
        top: '0px',
        left: '-56px',
        name: 'Independence Day',
        mark: 3.9,
        genre: ['Action', 'Sci-Fi'],
        id: 6
    },
    {
        url: '../images/ice-age.png',
        top: '0px',
        left: '-185px',
        name: 'Ice Age: Collisio...',
        mark: 4.5,
        genre: ['Adventure', 'Comedy'],
        id: 7
    },
    {
        url: '../images/flex_moana.png',
        top: '0px',
        left: '0px',
        name: 'Moana',
        mark: 4.9,
        genre: ['Action', 'Fantasy'],
        id: 8
    },
    {
        url: '../images/captain.png',
        top: '0px',
        left: '-138px',
        name: 'Captain America...',
        mark: 4.9,
        genre: ['Action', 'Adventure', 'Sci-Fi'],
        id: 9
    },
    {
        url: '../images/doctorstrange.png',
        top: '-155px',
        left: '0px',
        name: 'Doctor Strange',
        mark: 4.8,
        genre: ['Action', 'Adventure', 'Fantasy'],
        id: 10
    },
    {
        url: '../images/creed.png',
        top: '0px',
        left: '-221px',
        name: 'AssAssin’s Creed',
        mark: 4.2,
        genre: ['Action', 'Adventure', 'Fantasy'],
        id: 11
    },
    {
        url: '../images/fantastic_beasts.png',
        top: '-102px',
        left: '-34px',
        name: 'Fantastic Beasts...',
        mark: 4.7,
        genre: ['Adventure', 'Family', 'Fantasy'],
        id: 12
    }
];

export const Content = (props) => {
    let topItems = mainItems.slice(0, 8);
    let bottomItems = mainItems.slice(-4);
  return (
      <div className='content-container'>
          <div className='content-navbar'>
              <Navbar
                  liItems={['Trending', 'Top Rated', 'New Arrivals', 'Trailers', 'Coming Soon', 'Genre ']}
                  locationStyle={'content-navbar-style'}
              />
          </div>
          <MainItems
              mainItems={topItems}
          />
          <Banner />
          <MainItems
              mainItems={bottomItems}
          />
      </div>
  )
};