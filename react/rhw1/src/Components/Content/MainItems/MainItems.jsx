import React from "react";
import './MainItems.css';
import {FilmItem} from "../../Shared/FilmItem/FilmItem";

export const MainItems = (props) => {
    const mainItems = [...props.mainItems];
    let items = mainItems.map((item) => {
        return (
            <FilmItem
                key={item.id}
                url={item.url}
                top={item.top}
                left={item.left}
                name={item.name}
                mark={item.mark}
                genre={item.genre}
            />
        )
    });

    return (
        <div className={'main-items-container'}>
            {items}
        </div>
    )
};
