import React from "react";

export const SocialIcons = (props) => {
    let socialIconsNames = [...props.socialIconsNames];
    return (
        <div className='footer-icons'>
            {socialIconsNames.map(el => <i className={"fa fa-" + el + " social-icon"} aria-hidden="true"></i>)}
        </div>
    )
};