import React from 'react';
import './Button.css';

export const Button = (props) => {
    return (
        <button className={'general-button ' + props.colorStyle + ' ' + props.sizeStyle}>{props.text}</button>
    )
};