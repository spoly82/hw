import React from "react";
import './Mark.css';

export const Mark = (props) => {
    return (
        <span className='mark' style={{color: props.color}}>
            {props.mark}
        </span>
    )
};