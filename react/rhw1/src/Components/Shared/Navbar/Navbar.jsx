import React from "react";
import './Navbar.css'

export const Navbar = (props) => {
    let liItems = props.liItems.map(item => <li className='li-general-style'><a href="#" className={'a-general-style ' + props.locationStyle}>{item}</a></li>);
    return (
        <ul className='ul-general-style'>
            {liItems}
        </ul>
    )
};