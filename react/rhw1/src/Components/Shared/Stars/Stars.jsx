import React from "react";
import './Stars.css';

export const Stars = (props) => {
    let stars = [];
    for (let i = 0; i <= (props.stars-1); i++) {
        stars.push(1);
    };

    return (
        <div className='stars' style={{color: props.color, fontSize: props.fontSize}}>
            {stars.map(el => <i className="fa fa-star" aria-hidden="true"></i>) }
        </div>
    )
};