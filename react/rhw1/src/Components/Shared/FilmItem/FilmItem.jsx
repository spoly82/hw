import React from "react";
import './FilmItem.css';
import {Mark} from "../Mark/Mark";

export const FilmItem = (props) => {
    return (
        <div className='film-item-container'>
            <div className='film-item-img-container'>
                <img src={props.url}
                     className='film-item-img'
                     style={{top: props.top, left: props.left}} alt="item"/>
            </div>
            <div className={'item-description-container'}>
                <span className={'item-description-name'}>{props.name}</span>
                <div className={'item-description-mark'}>
                    <Mark
                        mark={props.mark}
                        color={'#0aaee4'}
                    />
                </div>
                <span className={'item-description-genre'}>{props.genre.join(', ')}</span>
            </div>
        </div>
    )
};