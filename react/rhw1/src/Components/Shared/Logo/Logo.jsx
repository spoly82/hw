import React from 'react';
import './Logo.css'

export const Logo = (props) => {
    return (
        <div className={'logo'} style={{color: props.color, fontSize: props.fontSize}}>
            <span className={'logo-text-bold'}>MOVIE</span>
            <span className={'logo-text-light'}>RISE</span>
        </div>
    )
};