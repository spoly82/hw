import React from "react";
import './Header.css';
import {Logo} from "../Shared/Logo/Logo";
import {Button} from "../Shared/Button/Button";
import {HeaderMovieDescription} from "./HeaderMovieDescription/HeaderMovieDescription";

export const Header = (props) => {
    const headerItem = {...props.headerItem};
    return (
        <header style={{backgroundImage: `url(${headerItem.url})`, backgroundSize: '100%'}}>
            <div className='header-container'>
                <div>
                    <Logo
                        color={'white'}
                        fontSize={'25px'}
                    />
                    <div className='header-top-buttons'>
                        <i className="fa fa-search search-icon" aria-hidden="true"></i>
                        <Button
                            colorStyle={'transparent-button'}
                            sizeStyle={'normal-size-button'}
                            text={'Sign in'}
                        />
                        <Button
                            colorStyle={'blue-button'}
                            sizeStyle={'normal-size-button'}
                            text={'Sign Up'}
                        />
                    </div>
                </div>
                <HeaderMovieDescription
                    name={headerItem.name}
                    genre={headerItem.genre}
                    filmLength={headerItem.filmLength}
                    mark={headerItem.mark}
                />
                <div className='header-bottom-buttons'>
                    <div className='firs-bottom-button'>
                        <Button
                            colorStyle={'blue-button'}
                            sizeStyle={'normal-size-button'}
                            text={'Watch Now'}
                        />
                    </div>
                    <Button
                        colorStyle={'white-border-button'}
                        sizeStyle={'normal-size-button'}
                        text={'View Info'}
                    />
                    <Button
                        colorStyle={'transparent-button'}
                        sizeStyle={'normal-size-button'}
                        text={'+ Favorites'}
                    />
                    <i className="fa fa-ellipsis-v more-icon" aria-hidden="true"></i>
                </div>
            </div>
        </header>
    )
};