import React from "react";
import './HeaderMovieDescription.css';
import {Stars} from "../../Shared/Stars/Stars";
import {Mark} from "../../Shared/Mark/Mark";

export const HeaderMovieDescription = (props) => {
    return (
        <div className={'header-description-section'}>
            <span className='header-description-name'>{props.name}</span>
            <span className='header-description-genre'>{props.genre.join(' ')} | {props.filmLength}</span>
            <Stars
                color={'#0aaee4'}
                fontSize={'20px'}
                stars={5}
            />
            <Mark
                mark={props.mark}
                color={'white'}
            />
        </div>
    )
};