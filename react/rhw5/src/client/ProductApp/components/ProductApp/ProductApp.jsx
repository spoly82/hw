import React from 'react';
import {ProductsList} from '../ProductsList';
import {ProductCart} from '../ProductCart';
import {useSelector} from 'react-redux';
import {Navbar} from "../../../../shared/components/Navbar";

export const ProductApp = () => {
    const cartIsOpen = useSelector(state => state.cartIsOpen);

    return (
        <>
            <Navbar/>
            <ProductsList/>
            {cartIsOpen ? <ProductCart/> : null}
        </>
    )
};
