import React from "react";
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {addToCartCreator, closeCartCreator, decrementProductCounterCreator, removeProductFromCartCreator} from "../../../../store/actionCreators";

export const ProductCart = () => {
    const productsAddedToCart = useSelector(state => state.productsAddedToCart, shallowEqual);
    const dispatch = useDispatch();

    let totalProductPrice = 0;
    if (productsAddedToCart.length > 0) {
        totalProductPrice = productsAddedToCart.reduce((sum, current) => sum + current.count*current.price, 0).toFixed(2);
    }

    const productsAddedToCartElements = productsAddedToCart.map(element =>
        <tr key={element.id}>
            <td>{element.name}</td>
            <td>{element.price}</td>
            <td>
                <div className="input-group">
                    <button className="minus-item input-group-addon btn btn-primary" onClick={() => dispatch(decrementProductCounterCreator(element))}>-</button>
                    <input type="number" className="item-count form-control" value={element.count}/>
                    <button className="plus-item btn btn-primary input-group-addon" onClick={() => dispatch(addToCartCreator(element))}>+</button>
                </div>
            </td>
            <td>
                <button className="delete-item btn btn-danger" onClick={() => dispatch(removeProductFromCartCreator(element))}>X</button>
            </td>
            <td>{element.price*element.count}</td>
        </tr>
    );

    return (
        <div >
            <div className="modal-overlay">
                <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Cart</h5>
                        <button type="button" className="close" onClick={() => dispatch(closeCartCreator())}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <table className="show-cart table">
                            {productsAddedToCartElements}
                        </table>
                        <div>Total price: ${totalProductPrice}<span className="total-cart"></span></div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={() => dispatch(closeCartCreator())}>Close</button>
                        <button type="button" className="btn btn-primary">Order now</button>
                    </div>
                </div>
            </div>
            </div>
        </div>
    )
};