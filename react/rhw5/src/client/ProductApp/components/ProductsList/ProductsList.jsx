import React from 'react'
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import {addToCartCreator} from "../../../../store/actionCreators";

export const ProductsList = () => {
    const products = useSelector(state => state.products, shallowEqual);
    const dispatch = useDispatch();
    const productsElements = products.map(element => {

        const handleClick = (e) => {
            e.preventDefault();
            dispatch(addToCartCreator(element))
        };

        return (
            <div className="col" key={element.id}>
                <div className="card" style={{width: 20 +"rem"}}>
                    <img className="card-img-additional" src={element.src} alt="Card image cap" />
                    <div className="card-block">
                        <h4 className="card-title">{element.name}</h4>
                        <p className="card-text">Price: ${element.price}</p>
                        <a href="#" className="add-to-cart btn btn-primary" onClick={handleClick}>Add to cart</a>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div className="container">
            <div className="row">
                {productsElements}
            </div>
        </div>
    )
};
