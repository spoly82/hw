import React, {useReducer} from 'react';
import './App.css';
import { ProductApp } from './client/ProductApp/components/ProductApp';
import {Provider} from 'react-redux';
import {persistor, store} from "./store/store";
import { PersistGate } from 'redux-persist/integration/react'

function App() {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <ProductApp/>
            </PersistGate>
        </Provider>
    );
}

export default App;


