import React from "react";
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {clearCartCreator, openCartCreator} from "../../../store/actionCreators";

export const Navbar = () => {
    const productsAddedToCart = useSelector(state => state.productsAddedToCart, shallowEqual);
    const dispatch = useDispatch();

    let totalProductCount = 0;
    if (productsAddedToCart.length > 0) {
        totalProductCount = productsAddedToCart.reduce((sum, current) => sum + current.count, 0);
    }

    return (
        <nav className="navbar navbar-inverse bg-inverse fixed-top bg-faded navbar-additional">
            <div className="row">
                <div className="col">
                    <button type="button" className="btn btn-primary" onClick={() => dispatch(openCartCreator())}>Cart (<span className="total-count">{totalProductCount}</span>)</button>
                    <button className="clear-cart btn btn-danger" onClick={() => dispatch(clearCartCreator())}>Clear Cart</button>
                </div>
            </div>
        </nav>
    )
};