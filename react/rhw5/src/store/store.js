import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { composeWithDevTools } from 'redux-devtools-extension';
import {changeCartList} from "./reducer";

const persistConfig = {
    key: 'cart',
    storage,
    whitelist: ['productsAddedToCart'],
};

const persistedReducer = persistReducer(persistConfig, changeCartList);

const store = createStore(persistedReducer, composeWithDevTools(
    applyMiddleware(),
));

const persistor = persistStore(store);

export {store, persistor};
