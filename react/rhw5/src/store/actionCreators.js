import {ADD_TO_CART, CLEAR_CART, CLOSE_CART, DECREMENT_PRODUCT_COUNT, OPEN_CART, REMOVE_FROM_CART} from "./actions";

export const openCartCreator = () => {
    return {
        type: OPEN_CART
    }
};

export const closeCartCreator = () => {
    return {
        type: CLOSE_CART
    }
};

export const addToCartCreator = (payload) => {
    return {
        type: ADD_TO_CART,
        payload
    }
};

export const decrementProductCounterCreator = (payload) => {
    return {
        type: DECREMENT_PRODUCT_COUNT,
        payload
    }
};

export const removeProductFromCartCreator = (payload) => {
    return {
        type: REMOVE_FROM_CART,
        payload
    }
};

export const clearCartCreator = () => {
    return {
        type: CLEAR_CART,
    }
};