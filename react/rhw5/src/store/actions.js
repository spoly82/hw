export const ADD_TO_CART = "ADD_TO_CART";
export const CLEAR_CART = "CLEAR_CART";
export const OPEN_CART = "OPEN_CART";
export const CLOSE_CART = "CLOSE_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const DECREMENT_PRODUCT_COUNT = "DECREMENT_PRODUCT_COUNT";


