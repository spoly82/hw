import {ADD_TO_CART, CLEAR_CART, CLOSE_CART, DECREMENT_PRODUCT_COUNT, OPEN_CART, REMOVE_FROM_CART} from './actions'

// let localItemsInCart = JSON.parse(localStorage.getItem('cart'));
// const productsAddedToCart = localItemsInCart || [];

const initialState = {
    products: [
        {
            name: "Orange",
            price: 0.5,
            src: "http://www.azspagirls.com/files/2010/09/orange.jpg",
            id: 1
        },
        {
            name: "Banana",
            price: 1.22,
            src: "http://images.all-free-download.com/images/graphicthumb/vector_illustration_of_ripe_bananas_567893.jpg",
            id: 2
        },
        {
            name: "Lemon",
            price: 5,
            src: "https://3.imimg.com/data3/IC/JO/MY-9839190/organic-lemon-250x250.jpg",
            id: 3
        },
    ],
    productsAddedToCart: [],
    cartIsOpen: false,
};

export const changeCartList = (state = initialState, action) => {

    switch (action.type) {
        case ADD_TO_CART:
            const isProductExistInCart = state.productsAddedToCart.findIndex(item => item.id === action.payload.id) !== -1;
            if(isProductExistInCart){
                return {
                    ...state,
                    productsAddedToCart: state.productsAddedToCart.map(item => item.id === action.payload.id ? {...item, count: item.count + 1} : {...item})
                }
            } else {
                return {
                    ...state,
                    productsAddedToCart: state.productsAddedToCart.concat({...action.payload, count: 1})
                }
            }
        case DECREMENT_PRODUCT_COUNT:
            const currentProductIndex = state.productsAddedToCart.findIndex(item => item.id === action.payload.id);
            if (state.productsAddedToCart[currentProductIndex].count > 1) {
                return {
                    ...state,
                    productsAddedToCart: state.productsAddedToCart.map(item => item.id === action.payload.id ? {...item, count: item.count - 1} : {...item})
                }
            } else {
                return {
                    ...state,
                    productsAddedToCart: state.productsAddedToCart.filter(item => item.id !== action.payload.id),
                };
            }
        case REMOVE_FROM_CART:
            return {
                ...state,
                productsAddedToCart: state.productsAddedToCart.filter(item => item.id !== action.payload.id),
            };
        case CLEAR_CART:
            return {
                ...state,
                productsAddedToCart: [],
            };
        case OPEN_CART:
            return {
                ...state,
                cartIsOpen: true,
            };
        case CLOSE_CART:
            return {
                ...state,
                cartIsOpen: false,
            };
        default:
            return state;
    }
};