import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import {ProductsOrderForm} from "./components/ProductsOrderForm/ProductsOrderForm";


function App() {
  return (
    <div className="App">
      <ProductsOrderForm />
    </div>
  );
}

export default App;
