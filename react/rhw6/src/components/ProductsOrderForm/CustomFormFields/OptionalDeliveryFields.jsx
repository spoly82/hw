import {useFormikContext} from "formik";
import {fields} from "../fields";
import React from "react";
import {TextInput} from "./TextInput";

export const OptionalDeliverylFields = () => {
    const { values } = useFormikContext();
    if (values.delivery === 'Новая почта') {
        return <TextInput {...fields.deliveryPost} />
    } else if (values.delivery === 'Курьерская доставка') {
        const courierFields = fields.deliveryCourier.map((fields) => <TextInput key={fields.id} {...fields} />);
        return <>{courierFields}</>
    }
    return null;
};