import React from "react";
import {useField} from "formik";
import InputMask from "react-input-mask";
import {TextInput} from "./TextInput";

export const MaskedInput = ({mask, ...props }) => {
    const [field] = useField(props);
    return (
        <div className="form-group">
            <InputMask
                {...field}
                {...props}
                mask={mask}
                onChange={field.onChange}
            >
                {(inputProps) => <TextInput {...inputProps}/>}
            </InputMask>
        </div>
    );
};