import React from "react";
import {useField} from "formik";

export const SelectInput = ({ options, ...props }) => {
    const [field, meta] = useField(props);
    const selectOptions = options.map(({value, text, id}) => <option value={value} key={id}>{text}</option>);

    return (
        <div className="form-group">
            <select className="form-control" {...field} {...props} >
                {selectOptions}
            </select>
            {meta.touched && meta.error ?
                <div className="invalid-feedback text-center">{meta.error}</div> :
                null}
        </div>
    );
};