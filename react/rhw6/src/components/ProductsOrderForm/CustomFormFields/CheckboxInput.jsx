import React from "react";
import {useField} from "formik";

export const CheckboxInput = ({ children, ...props }) => {
    const [field] = useField({ ...props});
    return (
        <div className="form-check">
            <input className="form-check-input" {...field} {...props} />
            <label className="form-check-label">
                {children}
            </label>
        </div>
    );
};