import React from "react";
import {useField} from "formik";

export const TextInput = ({ ...props }) => {
    const [field, meta] = useField(props);
    const valid = meta.touched && meta.error ? "is-invalid" : "";
    return (
        <div className="form-group">
            <input className={`form-control ${valid}`} {...field} {...props} />
            {meta.touched && meta.error ?
                <div className="invalid-feedback text-center">{meta.error}</div> :
                null}
        </div>
    );
};