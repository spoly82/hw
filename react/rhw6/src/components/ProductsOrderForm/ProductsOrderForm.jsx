import React from 'react';
import {Formik, Field, Form} from "formik";
import {fields} from "./fields";
import * as Yup from 'yup';

import {TextInput} from "./CustomFormFields/TextInput";
import {SelectInput} from "./CustomFormFields/SelectInput";
import {OptionalDeliverylFields} from "./CustomFormFields/OptionalDeliveryFields";
import {CheckboxInput} from "./CustomFormFields/CheckboxInput";
import {MaskedInput} from "./CustomFormFields/MaskedInput";
import {SubmitInput} from "./CustomFormFields/SubmitInput";

export const ProductsOrderForm = () => {

    const initialValues = {
        name: "",
        surname: "",
        patronymic: "",
        telephone: "",
        email: "",
        city: "",
        delivery: "",
        postBranch: "",
        street: "",
        house: "",
        apartment: "",
        deliveryTime: "",
        spam: false,
        comments: "",
    };

    const validate = values => {
        const errors = {};
        if (!values.delivery) {
            errors.delivery = 'обязательное поле';
        }
        if (values.delivery === 'Новая почта' && !values.postBranch) {
            errors.postBranch = 'обязательное поле';
        }
        if (values.delivery === 'Курьерская доставка' && !values.street) {
            errors.street = 'обязательное поле';
        }
        if (values.delivery === 'Курьерская доставка' && !values.house) {
            errors.house = 'обязательное поле';
        }
        if (values.delivery === 'Курьерская доставка' && !values.apartment) {
            errors.apartment = 'обязательное поле';
        }
        if (values.delivery === 'Курьерская доставка' && !values.deliveryTime) {
            errors.deliveryTime = 'обязательное поле';
        }
        if (!values.telephone) {
            errors.telephone = 'обязательное поле';
        } else if (values.telephone.includes('_')) {
            errors.telephone = 'необходимо ввести 9 цифр';
        }
        return errors;
    };

    const validationSchema = Yup.object({
        name: Yup.string().required('обязательное поле'),
        surname: Yup.string().required('обязательное поле'),
        patronymic: Yup.string().required('обязательное поле'),
        email: Yup.string().required('обязательное поле').email('обязательно должна быть @ и не должно быть пробелов'),
    });

    const onSubmit = (values)=> {
        console.log(values);
    };

    const formProps = {
        initialValues,
        validate,
        validationSchema,
        onSubmit
    };

    return (
        <Formik {...formProps}>
            <Form>
                <div className="mx-auto border rounded bg-light p-3 mt-2" style={{width: "500px"}}>
                    <TextInput {...fields.name} />
                    <TextInput {...fields.surname} />
                    <TextInput {...fields.patronymic} />
                    <MaskedInput mask={"+38(099)999-99-99"} {...fields.telephone} />
                    <TextInput {...fields.email} />
                    <SelectInput {...fields.city}/>
                    <SelectInput {...fields.delivery}/>
                    <OptionalDeliverylFields/>
                    <CheckboxInput {...fields.spam}>
                        подписаться на рассылку акций
                    </CheckboxInput>
                    <div className="form-group">
                        <Field className="form-control" as="textarea" name="comments"/>
                    </div>
                    <SubmitInput {...fields.submit} />
                </div>
            </Form>
        </Formik>
    );
};




