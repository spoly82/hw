export const fields = {
    name: {
        type: "text",
        name: "name",
        placeholder: "имя",
    },
    surname: {
        type: "text",
        name: "surname",
        placeholder: "фамилия"
    },
    patronymic: {
        type: "text",
        name: "patronymic",
        placeholder: "отчество"
    },
    telephone: {
        type: "tel",
        name: "telephone",
        placeholder: "телефон"
    },
    email: {
        type: "email",
        name: "email",
        placeholder: "e-mail"
    },
    city: {
        name: "city",
        options:  [
            {
                value: "",
                text: "выберите город",
                id: 1
            },
            {
                value: "Киев",
                text: "Киев",
                id: 2
            },
            {
                value: "Харьков",
                text: "Харьков",
                id: 3
            },
            {
                value: "Днепр",
                text: "Днепр",
                id: 4
            },
            {
                value: "Одесса",
                text: "Одесса",
                id: 5
            },
            {
                value: "Львов",
                text: "Львов",
                id: 6
            },
        ]},
    delivery: {
        name: "delivery",
        options:  [
            {
                value: "",
                text: "выберите способ доставки",
                id: 1
            },
            {
                value: "Новая почта",
                text: "Новая почта",
                id: 2
            },
            {
                value: "Курьерская доставка",
                text: "Курьерская доставка",
                id: 3
            },
        ]},
    deliveryPost: {
        type: "text",
        name: "postBranch",
        placeholder: "Номер отделения"
    },
    deliveryCourier: [
        {
            type: "text",
            name: "street",
            placeholder: "улица",
            id: 1
        },
        {
            type: "text",
            name: "house",
            placeholder: "дом",
            id: 2
        },
        {
            type: "text",
            name: "apartment",
            placeholder: "квартира",
            id: 3
        },
        {
            type: "text",
            name: "deliveryTime",
            placeholder: "время доставки",
            id: 4
        },
    ],
    spam: {
        type: "checkbox",
        name: "spam",
    },
    submit: {
        value: "Оплатить",
        type: "submit",
        name: "submit",
    }
};
