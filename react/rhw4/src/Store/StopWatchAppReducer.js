import {CHANGE_TIMER, RESET_TIMER} from './Actions'

export const initialState = {
    seconds: "00",
    tens: "00"
};

export const changeTimerStatus = (state, action) => {
    let newTens;
    let newSeconds;
    switch (action.type) {
        case CHANGE_TIMER:
            newTens = +state.tens+1;
            newSeconds = +state.seconds;
            if(newTens < 10) {
                newTens = "0" + newTens;
            }
            if (newTens > 99) {
                newSeconds = newSeconds+1;
                newTens = "00";
            }
            if (newSeconds < 10) {
                newSeconds = "0" + newSeconds;
            }
            if (newSeconds > 99) {
                newSeconds = "00";
            }
            return {
                ...state,
                seconds: newSeconds,
                tens: newTens,
            };
        case RESET_TIMER:
            return {
                ...state,
                seconds: "00",
                tens: "00",
            };
        default:
            break;
    }
};