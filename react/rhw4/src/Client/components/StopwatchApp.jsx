import React, {useReducer} from "react";
import {Button} from '../../Shared/components/Button'
import {CHANGE_TIMER, RESET_TIMER} from '../../Store/Actions'
import {changeTimerStatus, initialState} from '../../Store/StopWatchAppReducer'

let interval;

export const StopwatchApp = () => {
    const [state, dispatch] = useReducer(changeTimerStatus, initialState);

    const StartWatch = () => {
        interval = setInterval(() => dispatch({type: CHANGE_TIMER}), 10);
    };

    const StopWatch = () => {
        clearInterval(interval);
    };

    const ResetWatch = () => {
        clearInterval(interval);
        dispatch({type: RESET_TIMER})
    };

    return (
        <div className="wrapper">
            <h1>Stopwatch</h1>
            <h2>Vanilla JavaScript Stopwatch</h2>
            <p><span id="seconds">{state.seconds}</span>:<span id="tens">{state.tens}</span></p>
            <Button
                id="button-start"
                text="Start"
                handleClick={StartWatch}
            />
            <Button
                id="button-stop"
                text="Stop"
                handleClick={StopWatch}
            />
            <Button
                id="button-reset"
                text="Reset"
                handleClick={ResetWatch}
            />
        </div>
    )
};