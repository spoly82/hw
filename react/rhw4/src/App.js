import React from 'react';
import './App.css';
import Header from "./Components/Header/Header";
import ContentContainer from "./Components/Content/ContentContainer";

function App() {
  return (
      <div>
            <Header />
            <ContentContainer />
      </div>

  );
}

export default App;
