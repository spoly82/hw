import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import contentReducer from "./content-reducer";
import thunkMiddleware from "redux-thunk";

const reducers = combineReducers({
    contentPage: contentReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunkMiddleware)));
export default store;