import axios from "axios";

const TOGGLE_MODAL_ACTIVE = 'TOGGLE-MODAL-ACTIVE';
const SET_ITEMS = 'SET-ITEMS';

let initialState = {
    modalActive: false,
    currentItem: {},
    items: [],
};

const contentReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL_ACTIVE:
            return {
                ...state,
                modalActive: action.modalStatus,
                currentItem: action.currentItem
            };
        case SET_ITEMS:
            return {
                ...state,
                items: [...action.items]
            };
        default:
            return state
    }
};

export const changeModalActiveAC = (modalStatus, currentItem) => ({type: TOGGLE_MODAL_ACTIVE, modalStatus, currentItem});
export const setItemsAC = (items) => ({type: SET_ITEMS, items});

export const getItemsThunkCreator = () => {
    return (
        (dispatch) => {
            axios.get(`../itemsList.json`)
                .then(response => {
                    dispatch(setItemsAC(response.data.items));
                });


        }
    )
};
export default contentReducer;