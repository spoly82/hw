import React from 'react';

export const Button = ({id, handleClick, text}) => {
  return (
      <button id={id} onClick={handleClick}>{text}</button>
  )
};