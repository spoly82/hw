import React from 'react';
import {withStyles} from 'react-jss';

const styles = {
    itemCard: {
        width: 230,
        boxShadow: '0 0 10px rgba(0,0,0,0.5)',
        borderRadius: 5,
        marginBottom: 20,
        backgroundColor: 'white',
        position: 'relative',
        overflow: 'hidden',
        '& i': {
            marginLeft: 15,
            cursor: 'pointer'
        },
        '& p': {
            marginLeft: 15
        },
        '& button': {
            display: 'inline-block',
            float: 'right',
            marginRight: 15,
            marginTop: 18,
            fontFamily: 'Arial',
            fontSize: 10,
            color: 'white',
            backgroundColor: 'black',
            border: 'none',
            borderRadius: 3,
            padding: 6,
            cursor: 'pointer'
        }
    },
    itemImg: {
        width: '100%',
        height: 290,
        '& img': {
            width: '100%',
            borderRadius: 5,
        }
    },
    itemName: {
        fontFamily: 'Arial',
        fontWeight: 'bold',
        fontSize: 16,
        color: 'black',
        textAlign: 'left',
    },
    itemArticle: {
        display: 'inline-block',
        float: 'right',
        marginRight: 15
    },
    itemDescription: {
        fontFamily: 'Arial',
        fontSize: 13,
        color: 'black',
        textAlign: 'left',
    },
    itemPrice: {
        fontFamily: 'Arial',
        fontWeight: 'bold',
        fontSize: 18,
        color: 'black',
        textAlign: 'left',
        display: 'inline-block',
    },
    delete: {
        fontSize: 40,
        color: 'black',
        cursor: 'pointer',
        position: 'absolute',
        bottom: 8,
        right: 17
    }
};

const ItemCard = (props) => {
    const {classes} = props;

    return (
        <div className={classes.itemCard}>
            <div className={classes.itemImg}>
                <img src={props.URL} alt={'item'}/>
            </div>
            <p className={classes.itemName}>{props.name}</p>
            <i onClick={() => {
                props.handleAddToFavoritesClick(props.id)
            }}
               className="fa fa-star"
               aria-hidden="true"
               style={props.favorites ? {color: "orange"} : {color: "gray"}}/>
            <span className={classes.itemArticle}>{props.article}</span>
            <p className={classes.itemDescription}>Lorem ipsum dolor sit amet, con adipiscing elit, sed diam nonu.</p>
            <p className={classes.itemPrice}>{props.price} uah</p>
            {props.inCart
                ? <span className={classes.delete} onClick={() => {
                    props.handleAddToCartClick(props.id)
                }}>&times;</span>
                : <button onClick={() => {
                    props.handleAddToCartClick(props.id)
                }}>ADD TO CART</button>}
        </div>
    )
};

export default withStyles(styles)(ItemCard);