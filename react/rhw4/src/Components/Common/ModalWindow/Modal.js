import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'react-jss';

const styles = {
    modalOverley: {
        position: 'fixed',
        zIndex: 3,
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        overflow: 'auto',
        backgroundColor: 'rgba(0,0,0,0.8)'
    },
    modalWindow: {
        overflow: 'hidden',
        borderRadius: 5,
        width: 516,
        margin: '15% auto',

    },
    modalTitle: {
        fontFamily: 'Arial',
        fontSize: 22,
        color: 'white',
        textAlign: 'left',
        paddingLeft: 30,
        paddingTop: 24,
        paddingBottom: 24,
        display: 'inline-block'
    },
    close: {
        float: 'right',
        paddingRight: 20,
        fontSize: 40,
        color: 'white',
        cursor: 'pointer'
    },
    modalBody: {
        fontFamily: 'Arial',
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        padding: '35px 40px',
        lineHeight: 2,
    },
    modalFooter: {
        textAlign: 'center',
        paddingBottom: 25,
        '& button': {
            width: 101,
            height: 41,
            border: 'none',
            borderRadius: 5,
            fontSize: 15,
            color: 'white',
            marginRight: 10,
            cursor: 'pointer'
        }
    }
};

const Modal = (props) => {
    const {classes} = props;

    let modalButtons = props.actions.map((element) => {
        return (
            <button key={element.id}
                    className={element.action}
                    style={{backgroundColor: props.modalButtonBackgroundColor}}
                    onClick={() => props.onClick}>
                {element.text}
            </button>
        )
    });

    return (
        <>
            {props.modalActive &&
            <div className={classes.modalOverley + ' overley'}
                 onClick={props.onClick}>
                <div className={classes.modalWindow}
                     style={{backgroundColor: props.modalBackgroundColor}}>
                    <div className={classes.modalHeader}
                         style={{backgroundColor: props.modalHeaderBackgroundColor}}>
                        <div className={classes.modalTitle}>
                            {props.header}
                        </div>
                        {props.closeButton && <span className={classes.close}>&times;</span>}
                    </div>
                    <div className={classes.modalBody}>
                        {props.text}
                    </div>
                    <div className={classes.modalFooter}>
                        {modalButtons}
                    </div>
                </div>
            </div>}
        </>
    )
};

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
    closeButton: PropTypes.bool
};

export default withStyles(styles)(Modal);