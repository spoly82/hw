import React from 'react';
import {withStyles} from 'react-jss';
import {Link} from "react-router-dom";

const styles = {
    header: {
        height: 98,
        lineHeight: '98px',
        width: '100%',
        backgroundColor: 'darkmagenta',
        position: 'fixed',
        zIndex: 2,
        '& img': {
            height: '100%',
            marginRight: 5
        },
        '& span': {
            position: 'relative',
            top: -31,
            fontFamily: 'Arial',
            fontSize: 30,
            color: 'white',
            textAlign: 'left',
        },
        '& nav': {
            float: 'right',
            height: '100%',
            marginRight: 30
        }
    },
    item: {
        display: 'inline-block',
        paddingRight: 10,
        '& $a': {
            fontSize: 20,
            color: 'white',
            textDecoration: 'none',
        },
        '&:hover $a': {
            color: 'yellow',
        }
    },
    userAgentStylesheetReset: {
        marginBlockStart: 0,
    }
};

const Header = ({classes}) => {

    return (
        <header className={classes.header}>
            <img src={'https://cdn.images.express.co.uk/img/dynamic/143/590x/GAME-store-closures-list-1226696.jpg?r=1578665204705'} alt={'logo'}/>
            <span>.Store</span>
            <nav>
                <ul className={classes.userAgentStylesheetReset}>
                    <li className={classes.item}>
                        <Link to='/MainPage'>Home</Link>
                    </li>
                    <li className={classes.item}>
                        <Link to='/CartPage'>Cart</Link>
                    </li>
                    <li className={classes.item}>
                        <Link to='/FavoritesPage'>Favorites</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
};

export default withStyles(styles)(Header);