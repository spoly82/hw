import React from 'react';
import ItemCard from "../../Common/ItemCard/ItemCard";
import Modal from "../../Common/ModalWindow/Modal";
import {withStyles} from 'react-jss';

const styles = {
    wrapper: {
        margin: '0px auto',
        padding: '138px 0px',
        width: '80%',
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill, minmax(230px, 1fr))',
    },
    content: {
        backgroundColor: 'lightgrey',
    }
};

const MainPage = (props) => {

    const {classes} = props;
    let item = props.items.map((element) => {
        return (
            <ItemCard key={element.id}
                      id={element.id}
                      name={element.name}
                      article={element.article}
                      price={element.price}
                      URL={element.URL}
                      favorites={element.favorites}
                      inCart={element.inCart}
                      handleAddToFavoritesClick={props.handleAddToFavoritesClick}
                      handleAddToCartClick={props.handleAddToCartClick}/>
        )
    });

    return (
        <div className={classes.content}>
            <div className={classes.wrapper}>
                {item}
            </div>
            <Modal
                header={props.currentItem.inCart
                    ? 'Do you want to remove this from cart?'
                    : 'Do you want to add this to cart?'}
                text={props.currentItem.inCart
                    ? 'You are now removing ' + props.currentItem.name + ' from cart. Press OK to confirm'
                    : 'You are now adding ' + props.currentItem.name + ' to cart. Press OK to confirm'}
                closeButton={true}
                modalActive={props.modalActive}
                actions={[{action: 'confirm', text: 'Ok', id: 1}, {action: 'cancel', text: 'Cancel', id: 2}]}
                modalBackgroundColor={'DIMGRAY'}
                modalHeaderBackgroundColor={'DARKGRAY'}
                modalButtonBackgroundColor={'CornflowerBlue'}
                onClick={props.handleModalClick}
            />
        </div>
    )
};

export default withStyles(styles)(MainPage);