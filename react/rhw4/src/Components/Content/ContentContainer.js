import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MainPage from "./MainPage/MainPage";
import {Redirect, Route} from "react-router-dom";
import CartPage from "./CartPage/CartPage";
import FavoritesPage from "./FavoritesPage/FavoritesPage";
import {changeModalActiveAC, getItemsThunkCreator} from "../../Redux/content-reducer";
import {connect} from "react-redux";

const ContentContainer = (props) => {

    // const [items, setItems] = useState([]);
    const localItemsInCart = JSON.parse(localStorage.getItem('cart'));
    const [itemsInCart, setItemsInCart] = useState(localItemsInCart);
    const localFavoriteItems = JSON.parse(localStorage.getItem('favorites'));
    const [favoriteItems, setFavoriteItems] = useState(localFavoriteItems);

    useEffect(() => {
        props.setItemsThunk()
        // axios.get(`../itemsList.json`)
        //     .then(response => {
        //         let items = response.data.items;
        //         if (!favoriteItems) {
        //             setFavoriteItems([])
        //         } else {
        //             favoriteItems.forEach((item) => {
        //                 items = (items.map((element) => {
        //                     if (element.id === item.id) {
        //                         return {...element, favorites: true}
        //                     }
        //                     return element
        //                 }));
        //             });
        //         }
        //         if (!itemsInCart) {
        //             setItemsInCart([])
        //         } else {
        //             itemsInCart.forEach((item) => {
        //                 items = (items.map((element) => {
        //                     if (element.id === item.id) {
        //                         return {...element, inCart: true}
        //                     }
        //                     return element
        //                 }));
        //             });
        //         }
        //         setItems(items);
        //     });


    }, []);

    useEffect(() => {
        if (itemsInCart && itemsInCart.length !== 0) {
            localStorage.setItem('cart', JSON.stringify(itemsInCart));
        } else {
            localStorage.removeItem('cart');
        }
    }, [itemsInCart]);

    useEffect(() => {
        if (favoriteItems && favoriteItems.length !== 0) {
            localStorage.setItem('favorites', JSON.stringify(favoriteItems));
        } else {
            localStorage.removeItem('favorites');
        }
    }, [favoriteItems]);

    const handleAddToCartClick = (id) => {
        let currentItem = props.items.find(item => item.id === id);
        props.changeModalActive(true, currentItem)
    };

    const handleAddToFavoritesClick = (id) => {
        // setItems(items.map((element) => {
        //     if (element.id === id) {
        //         if (!element.favorites) {
        //             setFavoriteItems([...favoriteItems, {...element, favorites: true}]);
        //         } else {
        //             let currentFavoriteItem = favoriteItems.find(item => item.id === id);
        //             let index = favoriteItems.indexOf(currentFavoriteItem);
        //             let newFavoriteItems = favoriteItems.slice(0);
        //             newFavoriteItems.splice(index, 1);
        //             setFavoriteItems(newFavoriteItems);
        //         }
        //         return {...element, favorites: !element.favorites}
        //     }
        //     return element
        // }));
    };

    const handleModalClick = (e) => {
        if (e.target.classList.contains('overley') || e.target.classList.contains('cancel') || e.target.tagName === 'SPAN') {
            console.log('action canceled');
        } else if (e.target.classList.contains('confirm')) {
            // setItems(items.map((element) => {
            //     if (element.id === props.currentItem.id) {
            //         if (!element.inCart) {
            //             setItemsInCart([...itemsInCart, {...element, inCart: true}]);
            //         } else {
            //             let currentItemInCart = itemsInCart.find(item => item.id === props.currentItem.id);
            //             let index = itemsInCart.indexOf(currentItemInCart);
            //             let newItemsInCart = itemsInCart.slice(0);
            //             newItemsInCart.splice(index, 1);
            //             setItemsInCart(newItemsInCart);
            //         }
            //         return {...element, inCart: !element.inCart}
            //     }
            //     return element;
            // }));
        }
        props.changeModalActive(false, {});
    };

    return (
        <div className='content'>
            <Route exact path='/' render={() => <Redirect to={'/MainPage'}/>}/>
            <Route path='/MainPage' render={() =>
                <MainPage
                    modalActive={props.modalActive}
                    items={props.items}
                    currentItem={props.currentItem}
                    handleAddToCartClick={handleAddToCartClick}
                    handleAddToFavoritesClick={handleAddToFavoritesClick}
                    handleModalClick={handleModalClick}
                />}/>
            <Route path='/CartPage' render={() =>
                <CartPage
                    modalActive={props.modalActive}
                    items={itemsInCart}
                    currentItem={props.currentItem}
                    handleAddToCartClick={handleAddToCartClick}
                    handleAddToFavoritesClick={handleAddToFavoritesClick}
                    handleModalClick={handleModalClick}
                />}/>
            <Route path='/FavoritesPage' render={() =>
                <FavoritesPage
                    modalActive={props.modalActive}
                    items={favoriteItems}
                    currentItem={props.currentItem}
                    handleAddToCartClick={handleAddToCartClick}
                    handleAddToFavoritesClick={handleAddToFavoritesClick}
                    handleModalClick={handleModalClick}
                />}/>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        modalActive: state.contentPage.modalActive,
        currentItem: state.contentPage.currentItem,
        items: state.contentPage.items
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeModalActive: (modalStatus, currentItem) => {
            dispatch(changeModalActiveAC(modalStatus, currentItem))
        },
        setItemsThunk: () => {
            dispatch(getItemsThunkCreator())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContentContainer);