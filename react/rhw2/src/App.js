import React from 'react';
import './bootstrap.min.css'
import './App.css';
import {Container} from "./Components/Container/Container";

function App() {
  return (
    <Container/>
  );
}

export default App;
