import React from "react";

export const Input = (props) => {
    return (
        <input
            type={props.type}
            name={props.name}
            className={props.className}
            value={props.value}
            onChange={props.handleChange}
        />
    )
};