import React from "react";

export const Error = (props) => {
    return (
        <>
            {props.error ? <span style={{color: 'red'}}>{props.text}</span> : null}
        </>
    )
};