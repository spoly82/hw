import React from 'react';
import {InputForm} from "./InputForm/InputForm";
import {ItemContainer} from "./ItemContainer/ItemContainer";

export class Container extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            formFields: {
                title: {text: '', error: false},
                author: {text: '', error: false},
                isbn: {text: '', error: false},
            },
            booksAdded: [],
            id: 1
        };
    };

    handleChange = (e) => {
        this.setState({
            formFields: {
                ...this.state.formFields,
                [e.target.name]: {
                    ...this.state.formFields[e.target.name],
                    text: e.target.value
                }
            }
        });
    };

    handleFormError = () => {
        for (let key in this.state.formFields) {
            if (this.state.formFields[key].text === '') {
                this.setState((prevState) => {
                    return (
                        {
                            formFields: {
                                ...prevState.formFields,
                                [key]: {...prevState.formFields[key], error: true}
                            }
                        }
                    );
                });
            } else {
                this.setState((prevState) => {
                    return (
                        {
                            formFields: {
                                ...prevState.formFields,
                                [key]: {...prevState.formFields[key], error: false}
                            }
                        }
                    );
                });
            }
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state.formFields.title.text !== '' & this.state.formFields.author.text !== '' & this.state.formFields.isbn.text !== '') {
            let newBook = {
                title: this.state.formFields.title.text,
                author: this.state.formFields.author.text,
                isbn: this.state.formFields.isbn.text,
                id: this.state.id
            };
            this.setState((prevState) => {
                return (
                    {
                        booksAdded: [...this.state.booksAdded, newBook],
                        formFields: {
                            title: {text: '', error: false},
                            author: {text: '', error: false},
                            isbn: {text: '', error: false},
                        },
                        id: prevState.id + 1
                    }
                );
            });
        }
        this.handleFormError()
    };

    handleDelete = (id) => {
        const newBooksAdded = this.state.booksAdded.filter(
            (item) => item.id !== id
        );
        this.setState({ booksAdded: newBooksAdded });
    };

    handleUpdate = (obj) => {
        let currentBook = this.state.booksAdded.find(item => item.id === obj.id);
        let index = this.state.booksAdded.indexOf(currentBook);
        let newBooksAdded = this.state.booksAdded.slice(0);
        newBooksAdded.splice(index, 1, obj);
        this.setState({booksAdded: newBooksAdded});
    };

    render() {
        let bookAddedList = this.state.booksAdded.map((element) => {
            return (
                <ItemContainer
                    key={element.id}
                    id={element.id}
                    title={element.title}
                    author={element.author}
                    isbn={element.isbn}
                    handleDelete={this.handleDelete}
                    handleUpdate={this.handleUpdate}
                />
            )
        });

        return (
            <div className="container mt-4">
                <h1 className="display-4 text-center"><i className="fas fa-book-open text-primary"></i> <span className="text-secondary">Book</span> List</h1>
                <p className="text-center">Add your book information to store it in database.</p>
                <div className="row">
                    <div className="col-lg-4">
                        <InputForm
                            formFields={this.state.formFields}
                            handleSubmit={this.handleSubmit}
                            handleChange={this.handleChange}
                        />
                    </div>
                </div>
                <h3 id="book-count" className="book-count mt-5">Всего книг: {this.state.booksAdded.length}</h3>
                <table className="table table-striped mt-2">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>ISBN#</th>
                        <th>Title</th>
                    </tr>
                    </thead>
                    <tbody id="book-list">
                        {bookAddedList}
                    </tbody>
                </table>
            </div>
        )
    }
}