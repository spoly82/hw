import React from "react";
import {Input} from "../../Shared/Input";
import {Error} from "../../Shared/Error";

export class InputForm extends React.Component {
    render() {
        const { formFields } = this.props;
        return (
            <form id="add-book-form" onSubmit={this.props.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <Input
                        type="text"
                        name="title"
                        className="form-control"
                        value={formFields.title.text}
                        handleChange={this.props.handleChange}
                    />
                    <Error
                        error={formFields.title.error}
                        text="required!"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="author">Author</label>
                    <Input
                        type="text"
                        name="author"
                        className="form-control"
                        value={formFields.author.text}
                        handleChange={this.props.handleChange}
                    />
                    <Error
                        error={formFields.author.error}
                        text="required!"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="title">ISBN#</label>
                    <Input
                        type="text"
                        name="isbn"
                        className="form-control"
                        value={formFields.isbn.text}
                        handleChange={this.props.handleChange}
                    />
                    <Error
                        error={formFields.isbn.error}
                        text="required!"
                    />
                </div>
                <input type="submit" value="Add Book" className="btn btn-primary" />
            </form>
        )
    }
}