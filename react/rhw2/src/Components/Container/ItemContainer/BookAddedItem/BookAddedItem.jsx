import React from "react";

export class BookAddedItem extends React.Component {
    render() {
        return (
            <tr>
                <td>{this.props.title}</td>
                <td>{this.props.author}</td>
                <td>{this.props.isbn}</td>
                <td><a href="#" className="btn btn-info btn-sm btn-edit" onClick={this.props.toggleUpdateMode}><i className="fas fa-edit"></i></a></td>
                <td><a href="#" className="btn btn-danger btn-sm btn-delete" onClick={() => {this.props.handleDelete(this.props.id)}}>X</a></td>
            </tr>
        )
    }
}