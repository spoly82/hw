import React from 'react';
import {UpdateForm} from "./UpdateForm/UpdateForm";
import {BookAddedItem} from "./BookAddedItem/BookAddedItem";

export class ItemContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            updateMode: false,
            currentBookItem: {
                title: this.props.title,
                author: this.props.author,
                isbn: this.props.isbn,
                id: this.props.id
            },
        };
    };

    toggleUpdateMode = () => {
        this.setState((prevState) => {
            return { updateMode: !prevState.updateMode };
        });
    };

    handleUpdate = (e) => {
        e.preventDefault();
        let updatedBookItem = {
            ...this.state.currentBookItem
        };
        this.props.handleUpdate(updatedBookItem);
        this.toggleUpdateMode()
    };

    handleChange = (e) => {
        this.setState({
            currentBookItem: {
                ...this.state.currentBookItem,
                [e.target.name]: e.target.value
            }
        });
    };

    render() {
        return (
            <>
                {this.state.updateMode
                    ? <UpdateForm
                        title={this.state.currentBookItem.title}
                        author={this.state.currentBookItem.author}
                        isbn={this.state.currentBookItem.isbn}
                        handleUpdate={this.handleUpdate}
                        handleChange={this.handleChange}
                    />
                    : <BookAddedItem
                        id={this.state.currentBookItem.id}
                        title={this.state.currentBookItem.title}
                        author={this.state.currentBookItem.author}
                        isbn={this.state.currentBookItem.isbn}
                        handleDelete={this.props.handleDelete}
                        toggleUpdateMode={this.toggleUpdateMode}
                    />}
            </>
        )
    }
}