import React from "react";
import {Input} from "../../../Shared/Input";

export class UpdateForm extends React.Component {
    render() {
        return (
            <tr>
                <td className='additional-fix-td'>
                    <form onSubmit={this.props.handleUpdate}>
                        <div className="form-group additional-fix">
                            <Input
                                type="text"
                                name="title"
                                className="form-control"
                                value={this.props.title}
                                handleChange={this.props.handleChange}
                            />
                        </div>
                        <div className="form-group additional-fix">
                            <Input
                                type="text"
                                name="author"
                                className="form-control"
                                value={this.props.author}
                                handleChange={this.props.handleChange}
                            />
                        </div>
                        <div className="form-group additional-fix">
                            <Input
                                type="text"
                                name="isbn"
                                className="form-control"
                                value={this.props.isbn}
                                handleChange={this.props.handleChange}
                            />
                        </div>
                        <div className="additional-fix">
                            <Input
                                type="submit"
                                className="btn btn-primary"
                                value="Update"
                            />
                        </div>
                    </form>
                </td>
            </tr>
        )
    }
}