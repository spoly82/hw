class GameWAM {
    constructor() {
        this.PCScores = 0;
        this.PlayerScores = 0;
        this.allCelsList = Array.from(document.querySelectorAll('td'));
    }

    start() {
        document.querySelector('.restart').onclick = (event) => {
            this.restart();
        };
        document.querySelector('.level-select').onclick = (event) => {
            switch (event.target.className) {
                case 'easy':
                    this.timer = 1500;
                    this.generateRandom();
                    break;
                case 'normal':
                    Game.timer = 1000;
                    this.generateRandom();
                    break;
                case 'hard':
                    Game.timer = 500;
                    this.generateRandom();
                    break;
            }
        }
    }

    generateRandom() {
        this.markWinner();
        this.markLoser();
        this.random = Math.floor(Math.random() * (this.allCelsList.length - 1));
        this.checkWinner();
    }

    markCell() {
        this.allCelsList[this.random].classList.add("active");
        this.checkCells();
    }

    checkCells() {
        if (this.allCelsList.length > 1) {
            setTimeout(() => this.generateRandom(), this.timer);
        } else if (this.allCelsList[this.random]) {
            this.markLoser();
        }
    }

    markLoser () {
        if ((this.random || this.random == 0) && this.allCelsList[this.random].className == "active") {
        this.allCelsList[this.random].classList.remove("active");
        this.allCelsList[this.random].classList.add("looser");
        this.allCelsList.splice(this.random, 1);
        this.PCScores++;
        }
    }

    markWinner () {
        document.querySelector('table').onclick = (event) => {
            if (event.target.className == "active") {
                event.target.classList.remove("active");
                event.target.classList.add("winner");
                this.allCelsList.splice(this.random, 1);
                this.PlayerScores++;
            }
        }
    }

    checkWinner () {
        if (this.PCScores >= 50 || this.PlayerScores >= 50) {
            if (this.PCScores > this.PlayerScores) {
                alert ('PC wins! Please try again');
            } else {
                alert ('You are winner! Congratulations!')
            };
            document.querySelector('.restart').classList.remove("hidden");
            console.log(Game);
            return
        } else {
            this.markCell();
        }
    }

    restart () {
        this.PCScores = 0;
        this.PlayerScores = 0;
        this.allCelsList = Array.from(document.querySelectorAll('td'));
        for (let i = 0; i < this.allCelsList.length; i++) {
            this.allCelsList[i].classList.remove("looser");
            this.allCelsList[i].classList.remove("winner");
        }
        document.querySelector('.restart').classList.add("hidden");
        Game.start();
    }
}

Game = new GameWAM();
Game.start();
