const requestFilms = new XMLHttpRequest ();

requestFilms.open('GET', 'https://swapi.co/api/films/');
requestFilms.send();
requestFilms.responseType = 'json';
requestFilms.addEventListener('load', function () {

    let films = this.response;

    for (let i = 0; i < films.results.length; i++) {

        let div1 = document.createElement('div');

        div1.innerHTML = '<p> Episode: ' + films.results[i].episode_id + '</p>' + '<p>' + films.results[i].title + '</p>' + '<p> Plot: ' + films.results[i].opening_crawl + '</p>';
        document.body.append(div1);

        let div2 = document.createElement('div');

        div2.innerHTML = '<div class="bubbles"> <div class="bubble"> <div class="circleL"></div> </div> <div class="bubble"> <div class="circleL"></div> </div> <div class="bubble"> <div class="circleL"></div> </div> </div>'
        document.body.append(div2);

        let charactersList = films.results[i].characters;
        let count = 0;
        let charactersNames = [];

        for (let j = 0; j < charactersList.length; j++) {

            let requestCharacters = new XMLHttpRequest ();

            requestCharacters.open('GET', charactersList[j]);
            requestCharacters.responseType = 'json';
            requestCharacters.send();
            requestCharacters.onload = () => {
                count++;
                charactersNames.push(requestCharacters.response.name);
                if (count === charactersList.length) {
                    div2.innerHTML = '<p> Characters: ' + charactersNames + '</p>';
                }
            };
        }
    }
});




