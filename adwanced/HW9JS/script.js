const targetBtn = document.querySelector('#target-btn');

targetBtn.onclick = () => {
    document.cookie = "experiment=novalue; max-age=300";
    const user = 'new-user';
    if (getCookie(user)) {
        document.cookie = "new-user=false";
    } else {
        document.cookie = "new-user=true";
    }
    console.log(document.cookie);
};

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

