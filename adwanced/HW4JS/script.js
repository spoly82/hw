class Trello {

    createCol() {
        document.querySelector('.create-col').onclick = (event) => {
            let column = document.createElement('div');
            let title = document.querySelector('.title').value;
            if (title == '') {
                alert ('введите название задачи!')
            } else {
                column.classList.add('trello');
                document.body.append(column);

                let headerColumn = document.createElement('div');
                headerColumn.classList.add('header-col');
                headerColumn.innerHTML = `<span class="col-title">${title}</span><button class="sorter">...</button>`;
                column.append(headerColumn);

                let contentColumn = document.createElement('div');
                contentColumn.classList.add('content-col');
                column.append(contentColumn);

                let footerColumn = document.createElement('div');
                footerColumn.classList.add('footer-col');
                footerColumn.innerHTML = `<button class="create-card">добавить...</button>`;
                column.append(footerColumn);

                this.createCard();

            }
        };
    }

    createCard() {
        document.querySelector('body').onclick = (event) => {
            if (event.target.classList.contains("create-card")) {
                let currentCol = event.target.closest('.trello');
                let colCard = document.createElement('div');
                colCard.classList.add('col-card');
                colCard.innerHTML = '<input type="text"><input class="submit-btn" type="submit" value="+">';
                colCard.setAttribute("draggable", "true");
                currentCol.querySelector('.content-col').append(colCard);
                this.moveCard();
            }

            if (event.target.classList.contains("submit-btn")) {
                let currentCard = event.target.closest('.col-card');
                let cardText = currentCard.querySelector('input:first-child').value;
                currentCard.innerHTML = `<span>${cardText}</span>`;
            }

            if (event.target.classList.contains("sorter")) {
                let currentCol = event.target.closest('.trello');
                let cards = Array.from(currentCol.querySelectorAll('.col-card'));
                let contentCol = currentCol.querySelector('.content-col');
                if (cards.length > 1) {
                    cards.sort(function(a, b) {
                        if (a.innerText < b.innerText)
                            return -1;
                        if (a.innerText > b.innerText)
                            return 1;
                        return 0
                    });
                    contentCol.innerHTML = '';
                    for (let i = 0; i < cards.length; i++) {
                        contentCol.append(cards[i]);
                    }
                }
            }
        }
    }

    moveCard() {
        let cards = document.querySelectorAll('.col-card');

        cards[cards.length-1].addEventListener('dragstart', function (event) {
            dragSrcEl = this;
            event.dataTransfer.effectAllowed = 'move';
            event.dataTransfer.setData('text/html', this.innerHTML);
        });


        cards[cards.length-1].addEventListener('dragover', function (event) {
            event.preventDefault();
        });

        cards[cards.length-1].addEventListener('drop', function (event) {
            if (dragSrcEl != this) {
                dragSrcEl.innerHTML = this.innerHTML;
                this.innerHTML = event.dataTransfer.getData('text/html');
            }
        });
    }
}
let dragSrcEl = null;
let trello = new Trello();
trello.createCol();