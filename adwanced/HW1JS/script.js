/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger (size, stuffing) {
    
    if (size != Hamburger.SIZE_SMALL && size != Hamburger.SIZE_LARGE) {
        throw new HamburgerException ("выбран неверный размер!")
    }
    if (stuffing != Hamburger.STUFFING_CHEESE && stuffing != Hamburger.STUFFING_SALAD && stuffing != Hamburger.STUFFING_POTATO) {
        throw new HamburgerException ("выбрана неверная начинка!")
    }
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    name: 'маленький',
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    name: 'большой',
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    name: 'сыр',
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    name: 'салат',
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    name: 'картошка',
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'майонез',
    price: 15,
    calories: 0
};
Hamburger.TOPPING_SPICE = {
    name: 'приправа',
    price: 20,
    calories: 5
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
        throw new HamburgerException ("выбрана неверная добавка!")
    }

    for (var i = 0; i < this.topping.length; i++) {
        if (this.topping[i] == topping) {
            throw new HamburgerException ("уже добавлено, больше нельзя!");
        }
    }
    this.topping.push(topping);
    return "успешно добавлен";
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
        throw new HamburgerException ("выбрана неверная добавка!")
    }
    for (var i = 0; i < this.topping.length; i++) {
        if (this.topping[i] == topping) {
            this.topping.splice([i], 1);
            return "успешно удалена!";
        }
    }
    throw new HamburgerException ("нельзя удалить то, что уже удалено или не было добавлено ранее!")
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    if (this.topping.length > 0) {
        var toppingsCount = '';
        for (var i = 0; i < this.topping.length; i++) {
            toppingsCount += `${this.topping[i].name} `;
        }
        console.log(toppingsCount);
    } else {
        console.log("нет добавок!");
    }
    return this.topping
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
   return this.size.name;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing.name;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var totalPrice = this.size.price + this.stuffing.price;
    if (this.topping.length > 0) {
        for (var i = 0; i < this.topping.length; i++) {
            totalPrice += this.topping[i].price;
        }
    }
    return totalPrice;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var totalCalories = this.size.calories + this.stuffing.calories;
    if (this.topping.length > 0) {
        for (var i = 0; i < this.topping.length; i++) {
            totalCalories += this.topping[i].calories;
        }
    }
    return totalCalories;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.message = message;
}

var sweetHam = new Hamburger (Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
console.log(sweetHam);
var miniHam = new Hamburger (Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(miniHam);
console.log(sweetHam.getSize());
console.log(sweetHam.getStuffing());
console.log(sweetHam.getToppings());
console.log(`${sweetHam.calculatePrice()} тугриков`);
console.log(`${sweetHam.calculateCalories()} калорий`);
sweetHam.addTopping(Hamburger.TOPPING_MAYO);
sweetHam.addTopping(Hamburger.TOPPING_SPICE);
console.log(sweetHam.getToppings());
console.log(`${sweetHam.calculatePrice()} тугриков`);
console.log(`${sweetHam.calculateCalories()} калорий`);
sweetHam.removeTopping(Hamburger.TOPPING_SPICE);
console.log(sweetHam.getToppings());
console.log(`${sweetHam.calculatePrice()} тугриков`);
console.log(`${sweetHam.calculateCalories()} калорий`);
var errorHam = new Hamburger (Hamburger.SIZE_MEDIUM, Hamburger.STUFFING_CHEESE);