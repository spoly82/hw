fetch('https://swapi.co/api/films/')
    .then(response => response.json())
    .then(films => renderFilmsInfo(films));

function renderFilmsInfo(films) {
        // console.log(films);
        for (let i = 0; i < films.results.length; i++) {

            let mainInfo = document.createElement('div');
            mainInfo.innerHTML = '<p> Episode: ' + films.results[i].episode_id + '</p>' + '<p>' + films.results[i].title + '</p>' + '<p> Plot: ' + films.results[i].opening_crawl + '</p>';
            document.body.append(mainInfo);

            let loader = document.createElement('div');
            loader.innerHTML = '<div class="bubbles"> <div class="bubble"> <div class="circleL"></div> </div> <div class="bubble"> <div class="circleL"></div> </div> <div class="bubble"> <div class="circleL"></div> </div> </div>'
            document.body.append(loader);

            let charactersUrls = films.results[i].characters;
            let charactersNames = [];
            let requests = charactersUrls.map(url => fetch(url));
            Promise.all(requests)
                .then(responses => responses.forEach(
                    response => response.json()
                        .then(character => charactersNames.push(character.name))
                        .then(() => loader.innerHTML = '<p> <strong>Characters:</strong> ' + charactersNames + '</p>')
                ))
        }
}



