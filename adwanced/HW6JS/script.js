fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(posts => renderPosts(posts))
    .then(() => getUsers())
    .then(() => {
        contentChange();
    });

function contentChange() {
    document.querySelector('body').onclick = (event) => {
        if (event.target.classList.contains("deleteBtn")) {
            deletePost(event);
        } else if (event.target.classList.contains("changeBtn")) {
            changePost (event);
        }
    };
    createPost();
}
function renderPosts(posts) {
    posts.forEach(function (element) {
        let card = document.createElement('div');
        card.classList.add('card');
        card.setAttribute('data-user', `${element.userId}`);
        card.setAttribute('data-post', `${element.id}`);
        card.innerHTML = '<p class="title">' + element.title + '</p>' + '<p class="text">' + element.body + '</p>';
        document.querySelector('.container').append(card);
        let deleteBtn = document.createElement('button');
        deleteBtn.classList.add('deleteBtn');
        deleteBtn.innerHTML = 'delete';
        card.prepend(deleteBtn);
        let changeBtn = document.createElement('button');
        changeBtn.classList.add('changeBtn');
        changeBtn.innerHTML = 'change';
        changeBtn.setAttribute('onclick', 'show(\'block\')');
        card.prepend(changeBtn);
    });
}

function getUsers() {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(users => users.forEach(function (element) {
            renderUsers(element);
        }));
}

function renderUsers(element) {
    let cards = document.querySelectorAll('.card');
    for (let i = 0; i < cards.length; i++) {
        if (cards[i].getAttribute('data-user') == element.id && !cards[i].classList.contains("rendered")) {
            let name = document.createElement('p');
            name.innerHTML = element.name;
            cards[i].append(name);
            let email = document.createElement('p');
            email.innerHTML = element.email;
            cards[i].append(email);
            cards[i].classList.add('rendered');
        }
    }
}

function deletePost (event) {
    let postId = event.target.closest('.card').getAttribute('data-post');
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'DELETE'
    })
        .then(() => event.target.closest('.card').remove())
        .then(() => console.log('deleted'))
}

function createPost () {
    document.querySelector('.createPost').onclick = () => {
        document.querySelector('.filter').style.display = 'block';
        document.querySelector('.modal').style.display = 'block';
        document.querySelector('.savePost').onclick = (event) => {
            event.preventDefault();
            fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                body: JSON.stringify({
                    title: document.querySelector('.postTitle').value,
                    body: document.querySelector('.postText').value,
                    userId: 1
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
                .then(response => response.json())
                .then(result => addNewPost(result))
        };
    }
}

function addNewPost(element) {
    console.log(element);
    document.querySelector('.filter').style.display = 'none';
    document.querySelector('.modal').style.display = 'none';
    let card = document.createElement('div');
    card.classList.add('card');
    card.setAttribute('data-user', `${element.userId}`);
    card.setAttribute('data-post', `${element.id}`);
    card.innerHTML = '<p class="title">' + element.title + '</p>' + '<p class="text">' + element.body + '</p>';
    document.querySelector('.container').prepend(card);
    let deleteBtn = document.createElement('button');
    deleteBtn.classList.add('deleteBtn');
    deleteBtn.innerHTML = 'delete';
    card.prepend(deleteBtn);
    let changeBtn = document.createElement('button');
    changeBtn.classList.add('changeBtn');
    changeBtn.innerHTML = 'change';
    changeBtn.setAttribute('onclick', 'show(\'block\')');
    card.prepend(changeBtn);
    getUsers();
}

function changePost (event) {
    let currentCard = event.target.closest('.card');
    let postId = currentCard.getAttribute('data-post');
    let userId = currentCard.getAttribute('data-user');
    document.querySelector('.savePost').onclick = (event) => {
        event.preventDefault();
        fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
            method: 'PUT',
            body: JSON.stringify({
                id: postId,
                title: document.querySelector('.postTitle').value,
                body: document.querySelector('.postText').value,
                userId: userId
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(result => console.log(result))
            .then(() => renderChanges(currentCard))
    }
}

function renderChanges(currentCard) {
    document.querySelector('.filter').style.display = 'none';
    document.querySelector('.modal').style.display = 'none';
    currentCard.querySelector('.title').innerText = document.querySelector('.postTitle').value;
    currentCard.querySelector('.text').innerText = document.querySelector('.postText').value;
}

function show (state) {
    document.querySelector('.filter').style.display = state;
    document.querySelector('.modal').style.display = state;
};