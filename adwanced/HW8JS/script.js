const checkIPBtn = document.querySelector('button');
const ipRender = document.querySelector('.ip-render');

checkIPBtn.onclick = () => {
    getIP();
};

async function getIP() {
    let response = await fetch('https://api.ipify.org/?format=json');
    let ipAddress = await response.json();
    checkIP(ipAddress.ip);
}

async function checkIP(ipAddress) {
    let response = await fetch(`http://ip-api.com/json/${ipAddress}?fields=continent,country,region,city,district&lang=ru`);
    let ipChecked = await response.json();
    renderCheckedIP(ipChecked);
}

function renderCheckedIP(ipChecked) {
    let continent = document.createElement('p');
    continent.innerText = 'континент: ' + ipChecked.continent;
    let country = document.createElement('p');
    country.innerText = 'страна: ' + ipChecked.country;
    let region = document.createElement('p');
    region.innerText = 'регион: ' + ipChecked.region;
    let city = document.createElement('p');
    city.innerText = 'город: ' + ipChecked.city;
    let district = document.createElement('p');
    district.innerText = 'район: ' + ipChecked.district;
    ipRender.append(continent, country, region, city, district);
}
