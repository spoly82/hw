class Hamburger {
    constructor(size, stuffing) {
        try {
            if (size == undefined || stuffing == undefined) {
                throw new HamburgerException("выбраны не верные параметры гамбургера!");
            }
        } catch (e) {
            alert(e.message);
        }
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }

    addTopping(topping) {
        try {
            if (topping == undefined) {
                throw new HamburgerException("добавка не выбрана!");
            }
            if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
                throw new HamburgerException("выбрана неверная добавка!");
            }
            for (let i = 0; i < this.topping.length; i++) {
                if (this.topping[i] == topping) {
                    throw new HamburgerException("уже добавлено, больше нельзя!");
                }
            }
            this.topping.push(topping);
        } catch (e) {
            alert(e.message);
        }
    }

    removeTopping(topping) {
        try {
            if (topping == undefined) {
                throw new HamburgerException("добавка не выбрана!");
            }
            if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
                throw new HamburgerException("выбрана неверная добавка!");
            }
            for (var i = 0; i < this.topping.length; i++) {
                if (this.topping[i] == topping) {
                    this.topping.splice([i], 1);
                    return
                }
            }
            throw new HamburgerException ("нельзя удалить то, что уже удалено или не было добавлено ранее!")
        } catch (e) {
            alert(e.message);
        }
    }

    getToppings() {
        return this.topping
    }

    getSize() {
        return this.size;
    }

    getStuffing() {
        return this.stuffing;
    }

    calculatePrice() {
        let totalPrice = this.size.price + this.stuffing.price;
        if (this.topping.length > 0) {
            for (var i = 0; i < this.topping.length; i++) {
                totalPrice += this.topping[i].price;
            }
        }
        return totalPrice;
    };

    calculateCalories() {
        let totalCalories = this.size.calories + this.stuffing.calories;
        if (this.topping.length > 0) {
            for (var i = 0; i < this.topping.length; i++) {
                totalCalories += this.topping[i].calories;
            }
        }
        return totalCalories;
    };
}

class HamburgerException {
    constructor(message) {
        this.message = message;
    }
}


Hamburger.SIZE_SMALL = {
    name: 'маленький',
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    name: 'большой',
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    name: 'сыр',
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    name: 'салат',
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    name: 'картошка',
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'майонез',
    price: 15,
    calories: 0
};
Hamburger.TOPPING_SPICE = {
    name: 'приправа',
    price: 20,
    calories: 5
};

SweetHam = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
SweetHam.addTopping(Hamburger.TOPPING_MAYO);
SweetHam.addTopping(Hamburger.TOPPING_SPICE);
console.log(`${SweetHam.calculatePrice()} тугриков`);
console.log(`${SweetHam.calculateCalories()} калорий`);
SweetHam.removeTopping(Hamburger.TOPPING_SPICE);
console.log(SweetHam);
console.log(SweetHam.getToppings());
console.log(SweetHam.getSize().name);
console.log(SweetHam.getStuffing().name);
console.log(`${SweetHam.calculatePrice()} тугриков`);
console.log(`${SweetHam.calculateCalories()} калорий`);
errorHam = new Hamburger(Hamburger.SIZE_MEDIUM, Hamburger.STUFFING_CHEESE);